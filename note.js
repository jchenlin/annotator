var arrayNotes = [];

//Add a new notezzz
window.addEventListener("dblclick", function(event) {
	addNote(event);
	this.removeEventListener("dblclick",arguments.callee,false);
});

//Get the coordinates
function getCoords(event){
	var posx = 0;
	var posy = 0;
	if (!e) var e = window.event;
	if (e.pageX || e.pageY)     {
		posx = e.pageX;
		posy = e.pageY;
	}
	else if (e.clientX || e.clientY)    {
		posx = e.clientX + document.body.scrollLeft
		+ document.documentElement.scrollLeft;
		posy = e.clientY + document.body.scrollTop
		+ document.documentElement.scrollTop;
	} 
	var coord = [];
	coord.push(posx);
	coord.push(posy);
	return coord;
}

//Add note
function addNote(event){
	var coordinates = getCoords(event);

	var note = $('<textarea placeholder="Write here..."></textarea>');
	note.attr('name', 'mynote')
	.attr('maxlength', '500')
	.attr('rows', '8')
	.attr('cols', '30')
	.css('position', 'absolute')
	.css('left', coordinates[0] + "px")
	.css('top', coordinates[1] + "px")
	.css('z-index', '999999999')
	.css('border', '2px solid #999966')
	.css('padding', '5px')
	.css('background', '#FFFF33')
	.css('font-family', 'sans-serif');

	var minimize = $('<img/>');
	minimize.attr("src", "http://forums.naughtydog.com/t5/image/serverpage/image-id/45197iC62FC84091591D17/image-size/original?v=mpbl-1&px=-1")
	.attr("alt", "sup")
	.css('position', 'absolute')
	.css('left', coordinates[0] - 30 + "px")
	.css('top', coordinates[1] + "px")
	.css('height', "30px")
	.css('width', "20px")
	.css('z-index', '999999999');

	var deletebtn = $('<img/>');
	deletebtn.attr("src", "http://www.veryicon.com/icon/png/System/Must%20Have/Delete.png")
	.attr("alt", "sup")
	.css('position', 'absolute')
	.css('left', coordinates[0] - 30 + "px")
	.css('top', coordinates[1] + 35 + "px")
	.css('height', "20px")
	.css('width', "20px")
	.css('z-index', '999999999');

	var pin = $('<img/>');
	pin.attr("src", "http://forums.naughtydog.com/t5/image/serverpage/image-id/45197iC62FC84091591D17/image-size/original?v=mpbl-1&px=-1.jpg")
	.attr("alt", "sup")
	.css('position', 'absolute')
	.css('left', coordinates[0] + "px")
	.css('top', coordinates[1] + "px")
	.css('height', "30px")
	.css('width', "20px")
	.css('z-index', '999999999');

	$(minimize).click(function() {
		//hide note
		$(this).fadeOut(100);
		$(deletebtn).fadeOut(100);
		note.fadeOut(100);
		//show pin
		$(pin).fadeIn('fast', function() {
			$('body').append(pin);
		});
		//note.blur();
	});

	$(pin).click(function() {
		//hide pin
		$(this).fadeOut(100);
		//show note
		note.fadeIn('fast');
		note.focus();
		$(minimize).fadeIn('fast');
		$(deletebtn).fadeIn('fast');
	});

	$(deletebtn).click(function() {
		var result = confirm("Delete Note?");
		if (result == true) {
			pin.remove();
			note.remove();
			$(minimize).remove();
			$(deletebtn).remove();
		}
	});

	$('body').append(note);
	$('body').append(minimize);
	$('body').append(deletebtn);
	$(note).focus();

	/*
    note.blur(function(){
        $(this).hide('fast');

        var self = this;

        var pin = $('<img/>');
        pin.attr("src", "http://forums.naughtydog.com/t5/image/serverpage/image-id/45197iC62FC84091591D17/image-size/original?v=mpbl-1&px=-1")
        .attr("alt", "sup")
        .css('position', 'absolute')
        .css('left', coordinates[0] + "px")
        .css('top', coordinates[1] + "px")
        .css('height', "40px")
        .css('width', "30px")
        .css('z-index', '999999999');

        $('body').append(pin);

        // Hide pin, show text
        pin.on("click", function(event) {
            note.show("slow");
            $(this).hide(800);
        }); 

        pin.on("dblclick", function(){
            var a;
            var r=confirm("Delete Note?");
            if (r==true)
              {
              pin.remove();
              note.remove();
              }
        })
    });
	 */
}

