# README #

This README would normally document whatever steps are necessary to get your application up and running.

### This repository is an online researching tool prototype in the form of a Google Browser 

* Summary: This prototype was created as prototype to be presented for the U of T course CSC318 (The Design of Interactive Computational Media).
* Requires latest Google Chrome (Version 39.0.2171.71 m)

### Setup ###

1. Install Chrome.
2. Open the Chrome Menu and select the More tools->Extension option. You can also do this by going directly to chrome://extensions/ on your Chrome browser.
3. Enable Developer mode
4. Clone the annotator to a directory of your choice.
5. On the Chrome extensions page, click on Load unpacked extension. Browse to the directory where you cloned the annotator.


### Contact ###
Email jack.chenlin@mail.utoronto.ca for further inquiries.